# Finals Workshop

The main goal is to reacreate a production-like stack using Docker + Ansible

## Prerequisite

Install Vagrant and virtualbox
```bash
$ sudo apt install -y virtualbox vagrant

$ vagrant up
[...]

$ ssh vagrant@192.168.33.10
password: vagrant
```

## Tasks

- Using ansible install Docker on the Vagrant VM
- Using ansible create a custom docker network
- Using ansible run a traefik proxy
- Using ansible run (must be accessible through traefik):
    - your created resume image (from the docker hub) on the VM
    - [git tea](https://hub.docker.com/r/gitea/gitea)
