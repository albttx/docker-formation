# WORKSHOPS

## Requirements

```sh
# Install docker
$ curl -fsSL https://get.docker.com -o get-docker.sh
$ sh ./get-docker.sh

# Bonus make docker work for current user (no root)
$ sudo usermod -a -G docker $USER
```

## [Workshops 01](./01/README.md)
