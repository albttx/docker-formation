# WORKSHOP 03

## Exercice 01

Create a `docker-compose.yml` file containing 2 `alpine` containers.

The alpine container MUST stay awake on background

```bash
$ docker-compose up -d
Recreating 03_alpine02_1 ... done
Recreating 03_alpine01_1 ... done

$ docker-compose ps
    Name           Command       State   Ports
----------------------------------------------
03_alpine01_1   **************   Up           
03_alpine02_1   **************   Up           

$ docker-compose exec alpine01 ping -c 1 alpine02
PING alpine01 (172.18.0.3): 56 data bytes
64 bytes from 172.18.0.3: seq=0 ttl=64 time=0.035 ms

--- alpine01 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.035/0.035/0.035 ms
```

## Exercice 02

Run a docker-compose with 2 services.

- [Traefik proxy](https://hub.docker.com/_/traefik)
- Your template

You should be able to access on your template on `template.localhost`

:warning: Your website MUST not have exposed port, only traefik need to expose ports (80)

### BONUS:

Expose traefik frontpage on `traefik.localhost`
