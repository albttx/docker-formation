# WORKSHOP 02

## Exercice 01

Create a custom network name: `alpinenet`

## Exercice 02

Create 2 containers based on alpine image name:
  - alpine1
  - alpine2

You need to be able to communicate between using their container name

```bash
$ docker exec -it alpine1 ping -c 2 alpine2
PING alpine2 (172.19.0.3): 56 data bytes
64 bytes from 172.19.0.3: seq=0 ttl=64 time=0.054 ms
64 bytes from 172.19.0.3: seq=1 ttl=64 time=0.153 ms
```

## Exercice 03

- Create a volume `database`
- run a container and mount the volume in `/db`
- Write some data
- Delete the container
- Recreate the container, you should still have the data in `/db`
