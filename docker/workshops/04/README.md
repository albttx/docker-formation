# WORKSHOP 04

The Goal is to create CI/CD that will create and deploy your docker container to the gitlab registry

## Setup

- Create a [gitlab](https://gitlab.com) account
- Create a git repository looking like

```
$ ls -l1
Dockerfile
README.md
www
```

## Exercice 01

- Create a `.gitlab-ci.yml` that will:

  - Build the docker image with the tag as `${CI_COMMIT_SHORT_SHA}` when you push on any branch except `master` and a `tag`
  - Build the docker image and push it to the gitlab docker registry only on master and new tag with the tag latest
  - Build the docker image and push it with the `${CI_COMMIT_TAG}` when there is a new tag
