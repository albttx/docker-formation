# WORKSHOP 01

## Exercice 01

Run a simple [hello world](https://hub.docker.com/_/hello-world) containers

## Exercice 02

Expose the html template in [template](./template) into [http://localhost:8080](http://localhost:8080)

You're free of the web server to use

## Exercice 03

Build a docker image containing the template

You must be able to run the container without a volume

## Exercice 04

- :warning: You need a [docker hub](https://hub.docker.com) account

Push the image into the docker registry under `yourname/resume`
