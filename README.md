# Formation Docker and Ansible

[slides](https://docs.google.com/presentation/d/14jBu9hVtBk0YDTGE0d_uenwOPKOvzZemOHdMxegGsg8/edit?usp=sharing)

## Setup

- Install [docker](https://docker.com) and [docker-compose](https://docs.docker.com/compose/)

```bash
# Download Docker
$ curl -fsSL https://get.docker.com -o get-docker.sh
$ sh get-docker.sh

# Install docker-compose
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose

$ sudo usermod -aG docker $USER
# or
$ sudo useradd docker
```

- Install [ansible]()

``` bash
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo apt-add-repository --yes --update ppa:ansible/ansible
$ sudo apt install ansible

```

## Docker

## Ansible

### [Workshops](./ansible/workshops/README.md)
